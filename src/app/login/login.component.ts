import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

const data = { username : "sajid" , password : "sajid"} ;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup( {
    username: new FormControl(),
    password: new FormControl()
  })

  constructor( private router: Router){   }


  toggleSubmit(e:Event){
   e.preventDefault();

   if( data.username == this.loginForm.value.username && data.password == this.loginForm.value.password ){
    //  console.log('correct credential');
  this.router.navigate( [ 'home' ])

   }
   else{
     alert('Invalid Credentials !');
   }
   
  //  console.log(data.username , data.password);
  //  console.log(this.loginForm.value.username ,this.loginForm.value.password );

  }
  ngOnInit(): void {
  }

}
